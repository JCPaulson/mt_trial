<?php
/**
 * Template Name: Masonry 
 *
 * Template for displaying post in a masonry layout
 *
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
			elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
			else { $paged = 1; }

			query_posts( 'posts_per_page=7&paged=' . $paged );
            if ( have_posts() ) : ?>
            	<div id="container">
	            <?php while ( have_posts() ) : the_post(); ?>

	                <?php get_template_part( 'loop', 'masonry' ); ?> 
	                   
	            <?php endwhile; ?>
                </div> <!-- #container -->
            <?php endif;  ?>
			
			<div class="buttonhdr">
				<?php previous_posts_link( '<img src="/wp-content/uploads/2015/01/up.png">  Load older posts' ); ?>
			</div>
			<div class="buttonhdr">
				 <?php next_posts_link( '<img src="/wp-content/uploads/2015/01/down.png"> Load more posts' ); ?>

			</div>

			<?php wp_reset_query(); ?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->
	
<script type="text/javascript">

</script>

<?php get_footer(); ?>
