jQuery( document ).ready(function() {
	var $container = jQuery('#container');
	// initialize after all image are loaded to ensure no overlaping.
	$container.imagesLoaded( function() {
		$container.masonry({
		  columnWidth: 275,
		  itemSelector: '.item',
		  "gutter": 40
		});
	});
	jQuery('.fancybox').fancybox({
		padding: 0,
	});
});