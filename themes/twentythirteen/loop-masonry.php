
<div class="item <?php if ( in_category( 'categoryb' )) { ?> orange <?php } elseif ( in_category( 'categoryc' )) { ?> green <?php } elseif ( in_category( 'cutest-cat' )) { ?> blue <?php } else { } ?>">
	<!-- Item for masonry lay-out -->
	<a href="#inline<?php the_ID(); ?>" class="fancybox">
		<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'large' ); } ?>
		<span class="cat"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></span>
		<div class="titlehdr">
			<p><?php the_title(); ?></p>
		</div>
	</a>
	<!-- The inline content for the lightbox --> 
	<div id="inline<?php the_ID(); ?>" class="inline" style="display: none;">
		<?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'full' ); } ?>
		<div class="title">
			<h1><?php the_title(); ?></h1>
			<p class="meta">By <?php the_author_link(); ?> on <?php echo get_the_date('m.d.y'); ?></p>
		</div><?php the_content( ); ?>
		<span class="tag"> <?php echo get_the_tag_list( 'Tagged:', ',' ); ?></span>					
	</div> <!-- .inline -->
	
</div> <!-- .item -->